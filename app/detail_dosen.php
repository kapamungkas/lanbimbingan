<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_dosen extends Model
{
    //
    protected $dates =['deleted_at'];
    public function user(){
        return $this->hasOne('App\User', 'id_dosen', 'id');
    }
}
