<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class waktu_dosen extends Model
{
    //
    protected $dates =['deleted_at'];
    public function users(){
        return $this->belongsTo('App\User','id_dosen','id');
    }
}
