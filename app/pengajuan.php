<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pengajuan extends Model
{
    //
    protected $dates =['deleted_at'];
    public function users(){
        return $this->belongsTo('App\User','id_mahasiswa','id');
    }

    public function users_dosen(){
        return $this->belongsTo('App\User','id_dosen','id');
    }
}
