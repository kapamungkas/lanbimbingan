<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\pelaporan;

class PengaduanController extends Controller
{
    //
    public function index()
    {
        $data['page'] = 'Pengaduan';
        $data['slug'] = 'pengaduan';
        $data['content'] = pelaporan::select('name', 'des_pelaporan', 'pelaporans.status', 'pelaporans.id')
            ->join('pengajuans', 'id_pengajuan', 'pengajuans.id')
            ->join('users', 'users.id', 'id_dosen')
            ->get();
        return view('admin.pengaduan', $data);
    }

    public function pengaduanMahasiswa()
    {
        $data['page'] = 'Pengaduan';
        $data['slug'] = 'pengaduan';
        $data['content'] = pelaporan::select('name', 'des_pelaporan', 'pelaporans.status', 'pelaporans.id')
            ->join('pengajuans', 'id_pengajuan', 'pengajuans.id')
            ->join('users', 'users.id', 'id_dosen')
            ->where('id_mahasiswa', 3)
            ->get();
        return view('mahasiswa.pengaduan', $data);
    }

    public function processPengaduan(Request $request)
    {
        $data = pelaporan::Find($request->id);
        $data->status = "on process";
        $data->save();

        if ($data) {
            return redirect()->back()->with(['success' => 'Data berhasil diubah']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal diubah']);
        }
    }

    public function addLaporan(Request $request)
    {
        $data = new pelaporan;
        $data->id_pengajuan = $request->id_pengajuan;
        $data->des_pelaporan = $request->des_pelaporan;
        $data->save();

        if ($data) {
            return redirect()->back()->with(['success' => 'Data berhasil ditambahkan']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal ditambahkan']);
        }
    }

}
