<?php

namespace App\Http\Controllers;

use App\pelaporan;
use Illuminate\Http\Request;
use App\pengajuan;
use App\User;
use App\waktu_dosen;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $data['page'] = 'Dashboard';
        $data['slug'] = 'dashboard';
        $data['cdosen'] = User::join('role_user', 'user_id', 'id')
            ->where('role_id', 2)
            ->count();
        $data['cmahasiswa'] = User::join('role_user', 'user_id', 'id')
            ->where('role_id', 3)
            ->count();
        $data['content'] = Pengajuan::select('pengajuans.id', 'name', 'des_pengajuan', 'waktu_pengajuan', 'tanggal_pengajuan', 'status')
            ->join('users', 'users.id', 'id_dosen')
            ->limit(10)
            ->get();
        // var_dump($data['content']);
        return view('admin.dashboard', $data);
    }

    public function indexDosen()
    {
        $data['page'] = 'Dashboard';
        $data['slug'] = 'dashboard';
        $data['content'] = waktu_dosen::limit(10)
            ->where('id_dosen', 2)
            ->orderBy('id', 'desc')
            ->get();
        $data['absen'] = waktu_dosen::orderBy('id', 'desc')
            ->where('id_dosen', 2)
            ->first();
        return view('dosen.dashboard', $data);
    }

    public function indexMahasiswa()
    {
        $data['page'] = 'Dashboard';
        $data['slug'] = 'dashboard';
        $data['cdpengajuan'] = pengajuan::where('id_mahasiswa', 3)
            ->count();
        $data['cpelaporan'] = pelaporan::join('pengajuans','id_pengajuan','pengajuans.id')->where('id_mahasiswa', 3)
            ->count();
        $data['content'] = Pengajuan::limit(10)
            ->where('id_mahasiswa', 3)
            ->get();
        return view('mahasiswa.dashboard', $data);
    }
}
