<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\pengajuan;
use DB;

class PengajuanController extends Controller
{
    //
    public function index()
    {
        $data['page'] = 'Pengajuan';
        $data['slug'] = 'pengajuan';
        $data['content'] = Pengajuan::select('pengajuans.id', 'name', 'des_pengajuan', 'waktu_pengajuan', 'tanggal_pengajuan', 'status')
            ->join('users', 'users.id', 'id_dosen')
            ->limit(10)
            ->get();
        return view('admin.pengajuan', $data);
    }

    public function pengajuan()
    {
        $data['page'] = 'Pengajuan';
        $data['slug'] = 'pengajuan';
        $data['content'] = Pengajuan::limit(10)
            ->where('id_dosen', 2)
            ->get();
        return view('dosen.pengajuan', $data);
    }

    public function pengajuanMahasiswa()
    {
        $data['page'] = 'Pengajuan';
        $data['slug'] = 'pengajuan';
        $data['content'] = Pengajuan::limit(10)
            ->where('id_mahasiswa', 3)
            ->orderBy('id', 'desc')
            ->get();
        return view('mahasiswa.pengajuan', $data);
    }

    public function findPengajuan(Request $request)
    {
        $data = pengajuan::join('users', 'users.id', 'pengajuans.id_dosen')
            ->where('pengajuans.id', $request->input('id'))->first();
        return response()->json($data);
    }
    public function addPengajuan(Request $request)
    {
        $data = new pengajuan;
        $data->id_mahasiswa = $request->idmahasiswa;
        $data->id_dosen = $request->iddosen;
        $data->des_pengajuan = $request->des_pengajuan;
        $data->waktu_pengajuan = $request->waktu;
        $data->tanggal_pengajuan = $request->tanggal;
        $data->lokasi = $request->lokasi;
        $data->save();

        if ($data) {
            return redirect('mahasiswa/pengajuan/')->with(['success' => 'Data berhasil ditambahkan']);
        } else {
            return redirect('mahasiswa/pengajuan/')->with(['error' => 'Data gagal ditambahkan']);
        }
    }

    public function getPengajuan(Request $request)
    {
        $data = pengajuan::select('pengajuans.id', 'name', 'des_pengajuan', 'tanggal_pengajuan', 'waktu_pengajuan', 'lokasi')->join('users', 'users.id', 'id_mahasiswa')
            ->where('pengajuans.id', $request->input('id'))->first();
        return response()->json($data);
    }

    public function terimaPengajuan(Request $request)
    {
        $data = pengajuan::find($request->id);
        $data->status = "approved";
        $data->save();

        if ($data) {
            return redirect()->back()->with(['success' => 'Data berhasil diubah']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal diubah']);
        }
    }

    public function tolakPengajuan(Request $request)
    {
        $data = pengajuan::find($request->id);
        $data->status = "declined";
        $data->save();

        if ($data) {
            return redirect()->back()->with(['success' => 'Data berhasil diubah']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal diubah']);
        }
    }



    public function jamtersedia(Request $request){
        $id = $request->input('id');
        $data['tersedia'] = DB::select("SELECT MAX(HOUR(waktu_datang)) as jam FROM waktu_dosens WHERE waktu_dosens.id_dosen = '$id'");
        $data['bimbingan'] = DB::select("SELECT MAX(HOUR(waktu_pengajuan)) as jam FROM pengajuans WHERE pengajuans.id_dosen = '$id'");
        return json_encode($data);
    }
}
