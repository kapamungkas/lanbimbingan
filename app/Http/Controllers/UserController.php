<?php

namespace App\Http\Controllers;

use App\detail_dosen;
use App\detail_mahasiswa;
use App\keahlian;
use App\keahlian_dosen;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function getDataDosen()
    {
        $data['page'] = 'Dosen';
        $data['slug'] = 'dosen';
        $data['content'] = User::select('users.id','users.name','email','keahlian')
        ->limit(10)
            ->join('role_user', 'users.id', 'user_id')
            ->join('roles', 'roles.id', 'role_id')
            ->join('detail_dosens','id_dosen','users.id')
            ->where('role_id', 2)
            ->get();
        return view('mahasiswa.dosen', $data);
    }
    //admin
    public function users()
    {
        $data['page'] = 'Dosen';
        $data['slug'] = 'dosen';
        $data['content'] = User::limit(10)
            ->join('role_user', 'users.id', 'user_id')
            ->join('roles', 'roles.id', 'role_id')
            ->where('role_id', 1)
            ->get();
        return view('admin.users', $data);
    }
    public function dosen()
    {
        $data['page'] = 'Dosen';
        $data['slug'] = 'dosen';
        $data['content'] = User::limit(10)
            ->join('role_user', 'users.id', 'user_id')
            ->join('roles', 'roles.id', 'role_id')
            ->where('role_id', 2)
            ->get();

        $data['keahlian'] = keahlian_dosen::all();
        return view('admin.dosen', $data);
    }

    public function findDosen(Request $request)
    {
        // $data['page'] = 'Dosen';
        // $data['slug'] = 'dosen';
        $data = User::join('detail_dosens', 'id_dosen', 'users.id')
            ->where('users.id', $request->input('id'))->first();
        return response()->json($data);
    }
    public function mahasiswa()
    {
        $data['page'] = 'Dosen';
        $data['slug'] = 'dosen';
        $data['content'] = User::limit(10)
            ->join('role_user', 'users.id', 'user_id')
            ->join('roles', 'roles.id', 'role_id')
            ->where('role_id', 3)
            ->get();
        return view('admin.mahasiswa', $data);
    }

    public function addAdmin(Request $request)
    {
        $data = new User;
        $data->name = $request->nama;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();

        $userRole = DB::table('role_user')->insert(
            ['user_id' => $data->id, 'role_id' => 1]
        );

        if ($data && $userRole) {
            return redirect()->back()->with(['success' => 'Data berhasil ditambahkan']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal ditambahkan']);
        }
    }
    //end admin


    public function addDosen(Request $request)
    {
        $data = new User;
        $data->name = $request->nama;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();

        $userRole = DB::table('role_user')->insert(
            ['user_id' => $data->id, 'role_id' => 2]
        );

        $duser = new detail_dosen;
        $duser->id_dosen = $data->id;
        $duser->nip = $request->nik;
        $keahlian = implode(',',$request->keahlian);
        $duser->keahlian = $keahlian;
        $duser->save();

        if ($data && $userRole && $duser) {
            return redirect()->back()->with(['success' => 'Data berhasil ditambahkan']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal ditambahkan']);
        }
    }

    public function addMahasiswa(Request $request)
    {
        $data = new User;
        $data->name = $request->nama;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();

        $userRole = DB::table('role_user')->insert(
            ['user_id' => $data->id, 'role_id' => 3]
        );

        $duser = new detail_mahasiswa();
        $duser->id_mahasiswa = $data->id;
        $duser->nim = $request->nim;
        $duser->angkatan = $request->angkatan;
        $duser->save();

        if ($data && $userRole && $duser) {
            return redirect()->back()->with(['success' => 'Data berhasil ditambahkan']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal ditambahkan']);
        }

        return "tes";
    }

}
