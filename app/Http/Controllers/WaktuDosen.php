<?php

namespace App\Http\Controllers;

use App\waktu_dosen;
use Illuminate\Http\Request;

class WaktuDosen extends Controller
{
    //
    public function index(){
        $data['page'] = 'Absensi';
        $data['slug'] = 'absensi';
        $data['content'] = waktu_dosen::limit(10)
                            ->where('id_dosen',2)
                            ->orderBy('id','desc')
                            ->get();
        return view('dosen.absensi',$data);
    }

    public function riwayatabsen(){
        $data['page'] = 'Absensi';
        $data['slug'] = 'absensi';
        $data['content'] = waktu_dosen::limit(10)->get();
        return view('admin.absensi',$data);
    }

    public function addAbsen(Request $request){
        $data =  new waktu_dosen;
        $data->id_dosen = $request->id;
        $data->waktu_datang = date("Y-m-d h:i:s");
        $data->waktu_pulang = null;
        $data->save();

        if($data){
            return redirect()->back()->with(['success' => 'Anda berhasil melakukan check in']);
        }else{
            return redirect()->back()->with(['error' => 'Anda gagal melakukan check in']);
        }
    }

    public function outAbsen(Request $request){
        $data =  waktu_dosen::find($request->id);
        $data->waktu_pulang = date("Y-m-d h:i:s");;
        $data->save();

        if($data){
            return redirect()->back()->with(['success' => 'Anda berhasil melakukan check out']);
        }else{
            return redirect()->back()->with(['error' => 'Anda gagal melakukan check out']);
        }
    }
}
