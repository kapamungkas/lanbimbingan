<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $userId = Auth::id();
        $user = User::join('role_user', 'role_user.user_id', 'users.id')->where('users.id', $userId)->first();
        // var_dump();

        if ($user->role_id == '1') {
            return '/administrator';
        } elseif ($user->role_id == '2') {
            return '/dosen';
        } elseif ($user->role_id == '3') {
            return '/mahasiswa';
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
