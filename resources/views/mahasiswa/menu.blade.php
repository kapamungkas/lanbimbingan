<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="nav-item <?php if ($slug == 'dashboard') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('/mahasiswa')}}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item <?php if ($slug == 'dosen') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('mahasiswa/dosen')}}">
                <i class="material-icons">face</i>
                <p>Dosen</p>
            </a>
        </li>
        <li class="nav-item <?php if ($slug == 'pengajuan') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('mahasiswa/pengajuan')}}">
                <i class="material-icons">restore</i>
                <p>Pengajuan</p>
            </a>
        </li>
        <li class="nav-item pengaduan">
            <a class="nav-link" href="{{url('mahasiswa/pengaduan')}}">
                <i class="material-icons">contact_mail</i>
                <p>Pengaduan</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="material-icons">settings_power</i>
                <p>Logout</p>
            </a>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>


    </ul>
</div>