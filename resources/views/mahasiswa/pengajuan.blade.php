@extends('layout')

@section('sidebar')
@include('mahasiswa.menu')
@endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Data Pengajuan</h4>
                    <p class="card-category"> List pengajuan yang tercatat di sistem</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif

                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th>ID</th>
                                <th>Dosen</th>
                                <th>Jenis Pengajuan</th>
                                <th>Waktu</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach($content as $c)
                                <tr>
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->users_dosen->name}}</td>
                                    <td>{{$c->des_pengajuan}}</td>
                                    <td>{{$c->waktu_pengajuan}}</td>
                                    <td>{{$c->tanggal_pengajuan}}</td>
                                    <td>
                                        @if($c->status == "pending")
                                        <a href="#" class="btn btn-primary btn-sm" id="{{$c->id}}">Belum Direspon</a>
                                        @elseif($c->status == "approved")
                                        <a class="btn btn-success btn-sm " id="{{$c->id}}">Sudah Diterima</a>


                                        @elseif($c->status == "declined")
                                        <a class="btn btn-danger btn-sm " id="{{$c->id}}">Sudah Ditolak</a>
                                        @endif
                                        <a href="#" class="btn btn-danger btn-sm lihat" id="{{$c->id}}"><i class="fa fa-flag" aria-hidden="true"></i></a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modalLihat fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Buat Pengaduan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{url('mahasiswa/add-laporan')}}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            @csrf
                                <label class="bmd-label-floating">No Pengajuan</label>
                                <input type="text" class="form-control idpengajuan" name="id_pengajuan" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Des Pengajuan</label>
                                <input type="text" name="des_pengajuan" class="form-control despengajuan">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Nama Dosen</label>
                                <input type="text" class="form-control namadosen">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Keluhan</label>
                                <input type="text" class="form-control keluhan" name="des_pelaporan" autofocus>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <button type="submit" class="btn btn-danger">Laporkan</button>
            </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $('.lihat').on("click", function() {
            ids = $(this).attr('id')
            $.ajax({
                type: 'GET',
                url: '{{url("mahasiswa/find-pengajuan")}}',
                data: {
                    id: ids
                },
                dataType: 'json',
                success: function(data) {
                    $('.idpengajuan').val(ids)
                    $('.namadosen').val(data.name)
                    $('.despengajuan').val(data.des_pengajuan)
                }
            });

            $('.modalLihat').modal('show');
        });
    })
</script>
@endsection