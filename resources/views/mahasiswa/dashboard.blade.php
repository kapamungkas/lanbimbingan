@extends('layout') @section('sidebar') @include('mahasiswa.menu') @endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">content_copy</i>
                    </div>
                    <p class="card-category">Pengajuan</p>
                    <h3 class="card-title">{{$cdpengajuan}}
                        <small>Pengajuan</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i> Lihat Semua
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">content_copy</i>
                    </div>
                    <p class="card-category">Pengaduan</p>
                    <h3 class="card-title">{{$cpelaporan}}
                        <small>Pengaduan</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i> Lihat Semua
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-warning">
                    <h4 class="card-title">Riwayat Pengajuan</h4>
                    <p class="card-category">Berikut adalah list pengajuan yang tercatat</p>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                            <th>ID</th>
                            <th>Mahasiswa</th>
                            <th>Dosen</th>
                            <th>Jenis Pengajuan</th>
                            <th>Waktu</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            @foreach($content as $c)
                            <tr>
                                <td>{{$c->id}}</td>
                                <td>{{$c->users->name}}</td>
                                <td>{{$c->users_dosen->name}}</td>
                                <td>{{$c->des_pengajuan}}</td>
                                <td>{{$c->waktu_pengajuan}}</td>
                                <td>{{$c->tanggal_pengajuan}}</td>
                                <td class="text-primary">
                                    <a href="#" class="btn btn-primary btn-sm">Lihat</a>
                                    <a href="#" class="btn btn-success btn-sm">Ubah</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>