@extends('layout') @section('sidebar') @include('mahasiswa.menu') @endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Data Dosen</h4>
                    <p class="card-category"> List dosen yang terdaftar di sistem</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Keahlian
                                </th>
                                <th width="200">
                                    Aksi
                                </th>
                            </thead>
                            <tbody>
                                @foreach($content as $c)
                                <tr>
                                    <td>
                                        {{ $c->id}}
                                    </td>
                                    <td>
                                        {{ $c->name}}
                                    </td>
                                    <td>
                                        {{ $c->email}}
                                    </td>
                                    <td>
                                        <?php
                                        $keahlian = "";
                                        $keahlian = explode(',', $c->keahlian)
                                        ?>
                                        @foreach($keahlian as $k)
                                        <a class="warna">{{$k}}</a>
                                        @endforeach
                                    </td>
                                    <td class="text-primary">
                                        <a href="#" class="btn btn-primary btn-sm lihat" id="{{$c->id}}">Buat Pengajuan</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modalLihat fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{url('mahasiswa/add-pengajuan')}}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Nama Dosen</label>
                                <input type="text" class="form-control namadosen" name="namadosen" autofocus>
                                @csrf
                                <input type="hidden" name="iddosen" class="form-control iddosen">
                                <input type="hidden" name="idmahasiswa" class="form-control" value="3">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">NIP</label>
                                <input type="text" class="form-control nip">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Email</label>
                                <input type="email" class="form-control email">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Keahlian</label>
                                <input type="text" class="form-control keahlian">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr>
                            <p>Waktu terbanyak dosen tersedia dikampus jam: <a class="tersedia"></a>:00</p>
                            <p>Waktu terbanyak dosen menerima bimbingan jam: <a class="bimbingan"></a>:00</p>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Deskripsi Pengajuan</label>
                                <input type="text" name="des_pengajuan" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Tanggal</label>
                                <input type="date" name="tanggal" class="form-control tanggal">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Waktu</label>
                                <input type="time" name="waktu" class="form-control waktu">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Lokasi</label>
                                <input type="text" name="lokasi" class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Buat Pengajuan</button>
            </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        //ajax get dosen 
        $('.lihat').on("click", function() {
            ids = $(this).attr('id')
            $.ajax({
                type: 'GET',
                url: '{{url("mahasiswa/find-dosen")}}',
                data: {
                    id: ids
                },
                dataType: 'json',
                success: function(data) {
                    $('.iddosen').val(ids)
                    $('.namadosen').val(data.name)
                    $('.nip').val(data.nip)
                    $('.email').val(data.email)
                    $('.keahlian').val(data.keahlian)
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{url("jamtersedia")}}',
                data: {
                    id: ids
                },
                dataType: 'json',
                success: function(data) {
                    // $('.idpengajuan').val(ids)
                    // $('.namadosen').val(data.name)
                    // $('.despengajuan').val(data.des_pengajuan)
                    for (var item in data.tersedia) {
                        // alert(data.tersedia[item].jam);
                        $('.tersedia').html(data.tersedia[item].jam)
                    }

                    for (var item in data.bimbingan) {
                        // alert(data.bimbingan[item].jam);
                        $('.bimbingan').html(data.bimbingan[item].jam)
                    }
                }
            });
            $('.modalLihat').modal('show');
        });
    })
</script>
@endsection
<style>
    .warna {
        background-color: cadetblue;
        color: #fff !important;
        padding: 4px;
        font-size: 12px;
        margin-left: 10px;
        ;
        ;
    }
</style>