@extends('layout') @section('sidebar') @include('dosen.menu') @endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">content_copy</i>
                    </div>
                    <p class="card-category">Lakukan Absensi</p>
                    @if($absen->waktu_pulang == NULL)
                    <a href="{{ url('dosen/out-absensi/'.$absen->id) }}" class="btn btn-danger">CHECK OUT</a>
                    @else
                    <form action="{{url('dosen/add-absensi')}}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="2">
                        <button class="btn btn-success">CHECK IN</button>

                    </form>
                    @endif
                </div>
                <div class="card-footer">
                    <div class="stats" style="text-align:center;">
                        <!-- Material switch -->
                        <i class="material-icons">date_range</i> Anda Tidak Sedang Dikampus
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">content_copy</i>
                    </div>
                    <p class="card-category">Jam Kerja</p>
                    <h3 class="card-title">20
                        <small>Jam</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i> Lihat Semua
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-warning">
                    <h4 class="card-title">Riwayat Absensi</h4>
                    <p class="card-category">Berikut adalah list absens yang tercatat</p>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                            <th>ID</th>
                            <th>Nama Dosen</th>
                            <th>Waktu Mulai</th>
                            <th>Waktu Selesai</th>
                        </thead>
                        <tbody>

                            @foreach($content as $c)
                            <tr>
                                <td>{{$c->id}}</td>
                                <td>{{$c->users->name}}</td>
                                <td>{{$c->waktu_datang}}</td>
                                <td>{{$c->waktu_pulang}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>