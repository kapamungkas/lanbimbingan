@extends('layout')

@section('sidebar')
@include('dosen.menu')
@endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Data Pengajuan</h4>
                    <p class="card-category"> List pengajuan yang tercatat di sistem</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif

                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th>ID</th>
                                <th>Mahasiswa</th>
                                <th>Jenis Pengajuan</th>
                                <th>Waktu</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach($content as $c)
                                <tr>
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->users->name}}</td>
                                    <td>{{$c->des_pengajuan}}</td>
                                    <td>{{$c->waktu_pengajuan}}</td>
                                    <td>{{$c->tanggal_pengajuan}}</td>
                                    <td class="text-primary">
                                        @if($c->status == "pending")
                                        <a href="#" class="btn btn-primary btn-sm lihat" id="{{$c->id}}">Lihat</a>
                                        @elseif($c->status == "approved")
                                        <a class="btn btn-success btn-sm " id="{{$c->id}}">Sudah Diterima</a>
                                        <a href="#" class="btn btn-primary btn-sm detail" id="{{$c->id}}">Lihat</a>
                                        
                                        
                                        @elseif($c->status == "declined")
                                        <a class="btn btn-danger btn-sm " id="{{$c->id}}">Sudah Ditolak</a>
                                        <a href="#" class="btn btn-primary btn-sm detail" id="{{$c->id}}">Lihat</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Ubah -->
<div class="modal modalLihat fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Pengajuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group is-filled">
                                <label class="bmd-label-floating">Nama Mahasiswa</label>
                                <input type="text" class="form-control nama" name="nama" autofocus>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group is-filled">
                                <label class="bmd-label-floating">Topik Bimbingan</label>
                                <input type="text" class="form-control topik" name="topik">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group is-filled">
                                <label class="bmd-label-floating">Tanggal</label>
                                <input type="text" class="form-control tanggal" name="tanggal">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group is-filled">
                                <label class="bmd-label-floating">Waktu</label>
                                <input type="text" class="form-control waktu" name="waktu">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group is-filled">
                                <label class="bmd-label-floating">Lokasi</label>
                                <input type="text" class="form-control lokasi" name="lokasi">
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger tolak">Tolak</a>
                    <a class="btn btn-primary terima">Terima</a>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $('.lihat').on("click", function() {
            ids = $(this).attr('id')
            $.ajax({
                type: 'GET',
                url: '{{url("dosen/get-pengajuan")}}',
                data: {
                    id: ids
                },
                dataType: 'json',
                success: function(data) {
                    $('.nama').val(data.name)
                    $('.topik').val(data.des_pengajuan)
                    $('.tanggal').val(data.tanggal_pengajuan)
                    $('.waktu').val(data.waktu_pengajuan)
                    $('.lokasi').val(data.lokasi)
                    $(".terima").attr("href", "{{url('dosen/terima-pengajuan')}}/" + data.id);
                    $(".tolak").attr("href", "{{url('dosen/tolak-pengajuan')}}/" + data.id);
                }
            });
            $('.modalLihat').modal('show');
        });

        $('.detail').on("click", function() {
            ids = $(this).attr('id')
            $.ajax({
                type: 'GET',
                url: '{{url("dosen/get-pengajuan")}}',
                data: {
                    id: ids
                },
                dataType: 'json',
                success: function(data) {
                    $('.nama').val(data.id_mahasiswa)
                    $('.topik').val(data.des_pengajuan)
                    $('.tanggal').val(data.tanggal_pengajuan)
                    $('.waktu').val(data.waktu_pengajuan)
                    $('.lokasi').val(data.lokasi)
                    $('.modal-footer').hide()
                }
            });
            $('.modalLihat').modal('show');
        });
    })
</script>
@endsection
<style>
.btn{
    color:#fff !important;
}
</style>