<div class="sidebar-wrapper">
    <ul class="nav">
        <li class="nav-item <?php if ($slug == 'dashboard') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('/administrator')}}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-item <?php if ($slug == 'pengguna') {
                                echo "active";
                            } ?>">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#0" role="button" aria-haspopup="true" aria-expanded="false"><i class="material-icons">face</i>
                Pengguna</a>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{url('administrator/users')}}">Admin</a>
                <a class="dropdown-item" href="{{url('administrator/dosen')}}">Dosen</a>
                <a class="dropdown-item" href="{{url('administrator/mahasiswa')}}">Mahasiswa</a>
            </div>
        </li>
        <li class="nav-item <?php if ($slug == 'absensi') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('administrator/absensi')}}">
                <i class="material-icons">content_paste</i>
                <p>Absensi</p>
            </a>
        </li>
        <li class="nav-item <?php if ($slug == 'keahlian') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('administrator/keahlian')}}">
                <i class="material-icons">gavel</i>
                <p>Keahlian</p>
            </a>
        </li>
        <li class="nav-item <?php if ($slug == 'pengajuan') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('administrator/pengajuan')}}">
                <i class="material-icons">restore</i>
                <p>Pengajuan</p>
            </a>
        </li>
        <li class="nav-item <?php if ($slug == 'pengaduan') {
                                echo "active";
                            } ?>">
            <a class="nav-link" href="{{url('administrator/pengaduan')}}">
                <i class="material-icons">contact_mail</i>
                <p>Pengaduan</p>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="material-icons">settings_power</i>
                <p>Logout</p>
            </a>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>


    </ul>
</div>