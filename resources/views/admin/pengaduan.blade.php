@extends('layout')

@section('sidebar')
@include('admin.menu')

@endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Data Pengaduan</h4>
                    <p class="card-category"> List pengaduan yang tercatat di sistem</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif

                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Dosen
                                </th>
                                <th>
                                    Deskripsi
                                </th>
                                <th width="200">
                                    Status
                                </th>
                            </thead>
                            <tbody>
                                @foreach($content as $c)
                                <tr>
                                    <td>
                                        {{$c->id}}
                                    </td>
                                    <td>
                                        {{$c->name}}
                                    </td>
                                    <td>
                                        {{$c->des_pelaporan}}
                                    </td>
                                    <td class="text-primary">
                                        @if($c->status == "on process")
                                        <a href="" class="btn btn-success btn-sm">Sudah diproses</a>
                                        @else
                                        <a href="{{ url('administrator/process-pengaduan/'.$c->id) }}" class="btn btn-primary btn-sm">Proses Sekarang</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection