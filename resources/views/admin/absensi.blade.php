@extends('layout') @section('sidebar') @include('admin.menu') @endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-warning">
                    <h4 class="card-title">Riwayat Absensi</h4>
                    <p class="card-category">Berikut adalah list absens yang tercatat</p>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                            <th>ID</th>
                            <th>Nama Dosen</th>
                            <th>Waktu Mulai</th>
                            <th>Waktu Selesai</th>
                        </thead>
                        <tbody>

                            @foreach($content as $c)
                            <tr>
                                <td>{{$c->id}}</td>
                                <td>{{$c->users->name}}</td>
                                <td>{{$c->waktu_datang}}</td>
                                <td>{{$c->waktu_pulang}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection