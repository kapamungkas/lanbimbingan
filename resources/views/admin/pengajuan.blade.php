@extends('layout')

@section('sidebar')
@include('admin.menu')
@endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Data Pengajuan</h4>
                    <p class="card-category"> List pengajuan yang tercatat di sistem</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-primary">
                                <th>ID</th>
                                <th>Dosen</th>
                                <th>Jenis Pengajuan</th>
                                <th>Waktu</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach($content as $c)
                                <tr>
                                    <td>{{$c->id}}</td>
                                    <td>{{$c->name}}</td>
                                    <td>{{$c->des_pengajuan}}</td>
                                    <td>{{$c->waktu_pengajuan}}</td>
                                    <td>{{$c->tanggal_pengajuan}}</td>
                                    <td><a class="btn btn-sm btn-warning">{{$c->status}}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection