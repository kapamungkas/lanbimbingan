 @extends('layout') @section('sidebar') @include('admin.menu') @endsection @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">content_copy</i>
                    </div>
                    <p class="card-category">Dosen</p>
                    <h3 class="card-title">{{$cdosen}}
                        <small>Orang</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i> Lihat Semua
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">content_copy</i>
                    </div>
                    <p class="card-category">Mahasiswa</p>
                    <h3 class="card-title">{{ $cmahasiswa }}
                        <small>Orang</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i> Lihat Semua
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-warning">
                    <h4 class="card-title">Pengajuan Terbaru</h4>
                    <p class="card-category">Berikut adalah list pengajuan bimbingan terbaru</p>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                            <th>ID</th>
                            <th>Dosen</th>
                            <th>Jenis Pengajuan</th>
                            <th>Waktu</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                        @foreach($content as $c)
                            <tr>
                                <td>{{$c->id}}</td>
                                <td>{{$c->name}}</td>
                                <td>{{$c->des_pengajuan}}</td>
                                <td>{{$c->waktu_pengajuan}}</td>
                                <td>{{$c->tanggal_pengajuan}}</td>
                                <td><a class="btn btn-sm btn-warning">{{$c->status}}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection