<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route untuk user yang admin
// Route::group(['prefix' => 'admin', 'middleware' => ['auth','role:admin']], function(){
// 	Route::get('/', function(){
// 		// $data['users'] = \App\User::whereDoesntHave('roles')->get();
// 		return view('admin', $data);
// 	});
// });
// Route untuk user yang member

Auth::routes();
// , 'middleware' => ['auth','role:admin']
Route::get('/', function(){
    return redirect('login');
});
Route::get('/jamtersedia', 'PengajuanController@jamtersedia');
Route::group(['prefix' => 'administrator', 'middleware' => ['auth','role:admin']], function () {
    //Index dan user
    Route::get('/', 'DashboardController@index');
    Route::get('/users', 'UserController@users');
    Route::post('/add-user', 'UserController@addAdmin');
    Route::post('/add-dosen', 'UserController@addDosen');
    Route::post('/add-mahasiswa', 'UserController@addMahasiswa');
    Route::get('/dosen', 'UserController@dosen');
    Route::get('/mahasiswa', 'UserController@Mahasiswa');
    //Keahlian;
    Route::resource('keahlian', 'KeahlianController');
    //Pengajuan
    Route::get('/pengajuan', 'PengajuanController@index');
    //Pengaduan
    Route::get('/pengaduan', 'PengaduanController@index');
    Route::get('/process-pengaduan/{id}', 'PengaduanController@processPengaduan');
    //Absen
    Route::get('/absensi', 'WaktuDosen@riwayatabsen');
});
Route::group(['prefix' => 'dosen', 'middleware' => ['auth','role:dosen']], function () {
    Route::get('/', 'DashboardController@indexDosen');
    //pengajuan
    Route::get('/pengajuan', 'PengajuanController@pengajuan');
    Route::get('/get-pengajuan', 'PengajuanController@getPengajuan');
    Route::get('/terima-pengajuan/{id}', 'PengajuanController@terimaPengajuan');
    Route::get('/tolak-pengajuan/{id}', 'PengajuanController@tolakPengajuan');
    //absen
    Route::get('/absensi', 'WaktuDosen@index');
    Route::post('/add-absensi', 'WaktuDosen@addAbsen');
    Route::get('/out-absensi/{id}', 'WaktuDosen@outAbsen');
});
Route::group(['prefix' => 'mahasiswa', 'middleware' => ['auth','role:mahasiswa']], function () {
    Route::get('/', 'DashboardController@indexMahasiswa');
    Route::get('/dosen', 'UserController@getDataDosen');
    Route::get('/find-dosen', 'UserController@findDosen');
    Route::get('/pengajuan', 'PengajuanController@pengajuanMahasiswa');
    Route::post('/add-pengajuan', 'PengajuanController@addPengajuan');
    Route::get('/find-pengajuan', 'PengajuanController@findPengajuan');
    Route::get('/pengaduan', 'PengaduanController@pengaduanMahasiswa');
    Route::post('/add-laporan', 'PengaduanController@addLaporan');
});
