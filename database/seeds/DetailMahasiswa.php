<?php

use Illuminate\Database\Seeder;

class DetailMahasiswa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('detail_mahasiswas')->insert([
            [
                'id'  => 1,
                'id_mahasiswa' => 3,
                'nim'=>'15101148',
                'angkatan'=>'2015'
            ]
        ]);
    }
}
