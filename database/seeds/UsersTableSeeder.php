<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                'name'  => 'Admin',
                'email' => 'admin@gmail.com',
                'password'  => bcrypt('password')
            ],
            [
                'name'  => 'Wayan Wirta',
                'email' => 'dosen@gmail.com',
                'password'  => bcrypt('password')
            ],
            [
                'name'  => 'Kadek Agus Tina',
                'email' => 'mahasiswa@gmail.com',
                'password'  => bcrypt('password')
            ]
        ]);
    }
}
