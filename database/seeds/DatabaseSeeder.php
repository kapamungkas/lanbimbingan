<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UsersRoleTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PengajuanTableSeeder::class);
        $this->call(KeahlianTableSeeder::class);
        $this->call(PelaporanTableSeeder::class);
        $this->call(WaktuDosenSeeder::class);
        $this->call(DetailDosenSeeder::class);
        $this->call(DetailMahasiswa::class);
    }
}
