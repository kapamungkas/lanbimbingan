<?php

use Illuminate\Database\Seeder;

class PengajuanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pengajuans')->insert([
            [
                'id'  => 1,
                'id_mahasiswa' => 3,
                'id_dosen' => 2,
                'des_pengajuan' => 'Tugas akhir',
                'waktu_pengajuan' => '12:07:18',
                'tanggal_pengajuan' => '2019-11-20',
                'lokasi' => 'ruang dosen 1'
            ],
            [
                'id'  => 2,
                'id_mahasiswa' => 3,
                'id_dosen' => 2,
                'des_pengajuan' => 'Diskusi tugas',
                'waktu_pengajuan' => '02:07:18',
                'tanggal_pengajuan' => '2019-11-20',
                'lokasi' => 'ruang dosen 2'
            ],
            [
                'id'  => 3,
                'id_mahasiswa' => 3,
                'id_dosen' => 2,
                'des_pengajuan' => 'Menanyakan tentang kuliah',
                'waktu_pengajuan' => '13:07:18',
                'tanggal_pengajuan' => '2019-11-20',
                'lokasi' => 'Kantin Belakang'
            ]
        ]);
    }
}
