<?php

use Illuminate\Database\Seeder;

class WaktuDosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('waktu_dosens')->insert([
            [
                'id'  => 1,
                'id_dosen' => 2,
                'waktu_datang'=>'2019-11-08 01:00:00',
                'waktu_pulang'=>'2019-11-08 02:00:00',
            ],
            [
                'id'  => 2,
                'id_dosen' => 2,
                'waktu_datang'=>'2019-12-08 01:00:00',
                'waktu_pulang'=>'2019-12-08 02:00:00',
            ],
            [
                'id'  => 3,
                'id_dosen' => 2,
                'waktu_datang'=>'2019-12-08 01:00:00',
                'waktu_pulang'=>'2019-12-08 02:00:00',
            ]
        ]);
    }
}
