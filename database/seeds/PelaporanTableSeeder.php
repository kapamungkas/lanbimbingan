<?php

use Illuminate\Database\Seeder;

class PelaporanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pelaporans')->insert([
            [
                'id'  => 1,
                'id_pengajuan' => 1,
                'des_pelaporan'=>'Tidak Pernah datang saat bimbingan'
            ],
            [
                'id'  => 2,
                'id_pengajuan' => 2,
                'des_pelaporan'=>'Membatalkan janji tiba-tiba'
            ],[
                'id'  => 3,
                'id_pengajuan' => 3,
                'des_pelaporan'=>'Tidak mau memberi solusi'
            ]
        ]);
    }
}
