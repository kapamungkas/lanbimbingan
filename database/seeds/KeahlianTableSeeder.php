<?php

use Illuminate\Database\Seeder;

class KeahlianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('keahlian_dosens')->insert([
            [
                'id'  => 1,
                'nama_keahlian' => 'AI'
            ],
            [
                'id'  => 2,
                'nama_keahlian' => 'AUDIT'
            ],
            [
                'id'  => 3,
                'nama_keahlian' => 'Kelistrikan'
            ],
            [
                'id'  => 4,
                'nama_keahlian' => 'Pemrograman'
            ],
            [
                'id'  => 5,
                'nama_keahlian' => 'Sistem Informasi Manajemen'
            ]
        ]);
    }
}
