<?php

use Illuminate\Database\Seeder;

class DetailDosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('detail_dosens')->insert([
            [
                'id'  => 1,
                'id_dosen' => 2,
                'nip'=>'123123232',
                'keahlian'=>'AI,AUDIT'
            ]
        ]);
    }
}
